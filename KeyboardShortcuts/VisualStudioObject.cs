﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using EnvDTE;

namespace KeyboardShortcuts
{
    public class VisualStudioObject
    {
        private string progId;
        private object _activeObject;

        public static VisualStudioObject VisualStudio10 = new VisualStudioObject { progId = "VisualStudio.DTE.10.0" };
        public static VisualStudioObject VisualStudio12RC = new VisualStudioObject { progId ="VisualStudio.DTE.11.0" };

        public DTE GetObject()
        {
            return (DTE)Marshal.GetActiveObject(progId);
        }
    }
}
